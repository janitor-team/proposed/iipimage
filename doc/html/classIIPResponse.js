var classIIPResponse =
[
    [ "IIPResponse", "classIIPResponse.html#a7d78a2353ea15f09208796d812005d58", null ],
    [ "addResponse", "classIIPResponse.html#afb3b9be7408ea6772280c2cce3caaf02", null ],
    [ "addResponse", "classIIPResponse.html#a8f9416d8713947a34024635dec96f20f", null ],
    [ "addResponse", "classIIPResponse.html#a422eed5b71be621b3f1e02fb383f3be4", null ],
    [ "addResponse", "classIIPResponse.html#a9e4abc802176a20000ec48192b0408e4", null ],
    [ "addResponse", "classIIPResponse.html#a3c49d7be619367e7883ab66541be7b0d", null ],
    [ "errorIsSet", "classIIPResponse.html#a37cf31466a9202eefc096960137a6699", null ],
    [ "formatResponse", "classIIPResponse.html#affc95b730d60cf074635011dfd1ebcca", null ],
    [ "getAdvert", "classIIPResponse.html#a118b0447250be2ff270402c36da4b76e", null ],
    [ "getCacheControl", "classIIPResponse.html#ae0a1a8ef1951756d94708f7182154217", null ],
    [ "getCORS", "classIIPResponse.html#ad896028cfe38d7db6f0c50eaff0b978d", null ],
    [ "imageSent", "classIIPResponse.html#a31dcd66df236b7d1295063d873df906d", null ],
    [ "isSet", "classIIPResponse.html#a78d8852a3280b562f2d22507f23bdb99", null ],
    [ "setCacheControl", "classIIPResponse.html#aaa3a43cc1c42d4c2ed3d94028e1ce615", null ],
    [ "setCORS", "classIIPResponse.html#a1c5df5bfdff0e998284e42ccc7e6afef", null ],
    [ "setError", "classIIPResponse.html#a5b9059092a8f54c296e3ccc681e7fdee", null ],
    [ "setImageSent", "classIIPResponse.html#ab9ab55859fc05c60b6924b01f00e3ef3", null ],
    [ "setLastModified", "classIIPResponse.html#ab5b7e6db61ef585e7715b73a9fdf04bb", null ],
    [ "setProtocol", "classIIPResponse.html#ad8514e54aecbdedbe9c80e4a2dc5dd4a", null ]
];