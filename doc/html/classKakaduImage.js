var classKakaduImage =
[
    [ "KDU_READMODE", "classKakaduImage.html#ae38bdfee80f8ee9c993024918837f490", [
      [ "KDU_FAST", "classKakaduImage.html#ae38bdfee80f8ee9c993024918837f490a2e5991df9b83bf4927eaad2d65a25fb7", null ],
      [ "KDU_FUSSY", "classKakaduImage.html#ae38bdfee80f8ee9c993024918837f490a089f2b8dc378eb1bac90db23d605a25e", null ],
      [ "KDU_RESILIENT", "classKakaduImage.html#ae38bdfee80f8ee9c993024918837f490a2b092671045e62a86480aef882f00021", null ]
    ] ],
    [ "KakaduImage", "classKakaduImage.html#a0e8c3b55c7f276a1fc4ce4eb366f5e9b", null ],
    [ "KakaduImage", "classKakaduImage.html#a1ec69ac644a1544001b9cb9eec3ae6df", null ],
    [ "KakaduImage", "classKakaduImage.html#aeaa4a2b3830dff0c5ede621990b8473b", null ],
    [ "KakaduImage", "classKakaduImage.html#af6a8aa9a344cb32af4a88a05f17b8e4d", null ],
    [ "~KakaduImage", "classKakaduImage.html#a9d5b6087f11ab192b0fdc735322d3f67", null ],
    [ "closeImage", "classKakaduImage.html#ac17d73fa593fbc280b305bbfb92e1716", null ],
    [ "getRegion", "classKakaduImage.html#afc9a84e1ce8c302ca96572ebc5f5c456", null ],
    [ "getTile", "classKakaduImage.html#aafb16ba604d6d20b3cf6339451e3ede2", null ],
    [ "loadImageInfo", "classKakaduImage.html#a9ad91aef16d4079bc0e47d9aa658299f", null ],
    [ "openImage", "classKakaduImage.html#a954a0c96187d3161daa2f6219059df37", null ],
    [ "operator=", "classKakaduImage.html#afb22dcbefd4567a45c5128f748831926", null ],
    [ "regionDecoding", "classKakaduImage.html#a0ed5ddf9e02b401fcf8dbf24838b7d95", null ],
    [ "kdu_readmode", "classKakaduImage.html#a043b1b8e649808c1d1b6bb174ca48731", null ]
];