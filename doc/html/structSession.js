var structSession =
[
    [ "codecOptions", "structSession.html#ad89bc324ac0b0d50bd13151d17b70d1d", null ],
    [ "headers", "structSession.html#a5c2a0779f1a83727da62b8bf4a3ac9dd", null ],
    [ "image", "structSession.html#a9ee907cb1d8920e7525812db15af3e62", null ],
    [ "imageCache", "structSession.html#af8ee46cc45d4ecc0bd18cd1ebae4cb7d", null ],
    [ "jpeg", "structSession.html#a4fd39c37fd8578a3651da988c5f3bff9", null ],
    [ "logfile", "structSession.html#a1671c1ceedebdbbf1511b602872ceb78", null ],
    [ "loglevel", "structSession.html#a89dd559fe87021ab609d422d018c4832", null ],
    [ "out", "structSession.html#a87f2af4f8ea4f710d1da753d0d5394cb", null ],
    [ "processor", "structSession.html#a266af2befd208503e83b242463b375c0", null ],
    [ "response", "structSession.html#a348c743b4579a7f53a1a0afe0c3ec994", null ],
    [ "tileCache", "structSession.html#a7a4b94994484d8a79d12e65bce04a952", null ],
    [ "view", "structSession.html#a8c71a4784d38102696c648a39f700356", null ],
    [ "watermark", "structSession.html#a4e34996b4e2676dd681c070e506a0802", null ]
];