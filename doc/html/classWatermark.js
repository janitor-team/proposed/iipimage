var classWatermark =
[
    [ "Watermark", "classWatermark.html#a274317a27cb523b519b28be6d622f2bf", null ],
    [ "Watermark", "classWatermark.html#a96a43470184588ea58d0b529213a7ec7", null ],
    [ "~Watermark", "classWatermark.html#a5e484cce31c3d67dceb751df8e48a8ef", null ],
    [ "apply", "classWatermark.html#a32e861948948b0ef070cb1a60439f58f", null ],
    [ "getImage", "classWatermark.html#af18c0054dceec80c7ab1a5d2c8d10967", null ],
    [ "getOpacity", "classWatermark.html#a497b6518f7470e5048bb6be853d4f85e", null ],
    [ "getProbability", "classWatermark.html#a7fe56ff07579dc3b432dc6daa699f25c", null ],
    [ "init", "classWatermark.html#afae57d5f76c3f9c813a56cc39dd10604", null ],
    [ "isSet", "classWatermark.html#ac52684304e9900866b0e890ca68257f1", null ]
];