var classOpenJPEGImage =
[
    [ "OpenJPEGImage", "classOpenJPEGImage.html#a81c1d8f62cfa2815def1aa6240d35532", null ],
    [ "OpenJPEGImage", "classOpenJPEGImage.html#a59293faa979675ddda209e5eaa53a40d", null ],
    [ "OpenJPEGImage", "classOpenJPEGImage.html#ad065c76671b81386c3260ba82109ce9e", null ],
    [ "OpenJPEGImage", "classOpenJPEGImage.html#ae56256c713841596bee85e1bf491e8fb", null ],
    [ "~OpenJPEGImage", "classOpenJPEGImage.html#aa15076cc6f3ab68c380273061381f8c2", null ],
    [ "closeImage", "classOpenJPEGImage.html#a5e1b02fe0f290f2d528785871584c269", null ],
    [ "getRegion", "classOpenJPEGImage.html#a6bc2f74edbb15f34214288832ca4fd78", null ],
    [ "getTile", "classOpenJPEGImage.html#a3c7ba5b04ed3b721a8fd96c702be8632", null ],
    [ "loadImageInfo", "classOpenJPEGImage.html#a2dcb234ef782b6d21f971a5656a70137", null ],
    [ "openImage", "classOpenJPEGImage.html#a7a0d97d713918940002dc32d512d1be0", null ],
    [ "regionDecoding", "classOpenJPEGImage.html#a352d3628ecec9f7d63fdca2997ababa8", null ]
];