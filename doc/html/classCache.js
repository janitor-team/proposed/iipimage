var classCache =
[
    [ "Cache", "classCache.html#a0947492d8c2a38b588185e2213f4f6ed", null ],
    [ "~Cache", "classCache.html#af8b171a6c49d88d3ba179477484b9d48", null ],
    [ "clear", "classCache.html#aaadc276ecc25d2e9270410c36d2a733e", null ],
    [ "getIndex", "classCache.html#ab33f2789c54cf9b07625ad566f079ade", null ],
    [ "getMemorySize", "classCache.html#a2398d519f59fe27b278e5ab2cd9d9b50", null ],
    [ "getNumElements", "classCache.html#a5cd9c3810888ad2fb784491ee9961d0e", null ],
    [ "getTile", "classCache.html#adf2e22afaf201cfeaef52f1bb1e78c8c", null ],
    [ "insert", "classCache.html#aa9974a22953e338a60f37ef92e5937fa", null ]
];