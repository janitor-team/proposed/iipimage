var classJPEGCompressor =
[
    [ "JPEGCompressor", "classJPEGCompressor.html#a075720ce0e74f48b1d48498039da2539", null ],
    [ "Compress", "classJPEGCompressor.html#a5f543bff5bfa528fdc5d6837f6553840", null ],
    [ "CompressStrip", "classJPEGCompressor.html#a77dacb7508b13ca5985bb30de14680d7", null ],
    [ "Finish", "classJPEGCompressor.html#aa1e096e2a3170221c36bd9a4ebf7b526", null ],
    [ "getHeader", "classJPEGCompressor.html#ae8dd617d645caabaa5fad022a24331c2", null ],
    [ "getHeaderSize", "classJPEGCompressor.html#a62fd19a3b193d907f796cc53d1fc8919", null ],
    [ "getMimeType", "classJPEGCompressor.html#a3068b12a9ed2935babbede68d4415a91", null ],
    [ "getQuality", "classJPEGCompressor.html#ac747bb7eaf4c82a50b1ba5127d46eeaa", null ],
    [ "getSuffix", "classJPEGCompressor.html#a97df480d52621b38e7275c6ff7266498", null ],
    [ "InitCompression", "classJPEGCompressor.html#a35af7be50cb146c028b5c09d378b54c0", null ],
    [ "setQuality", "classJPEGCompressor.html#a60a3aa47f982f64374ccff5b073c0011", null ]
];