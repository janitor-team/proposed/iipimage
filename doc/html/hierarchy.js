var hierarchy =
[
    [ "Cache", "classCache.html", null ],
    [ "Compressor", "classCompressor.html", [
      [ "JPEGCompressor", "classJPEGCompressor.html", null ]
    ] ],
    [ "Environment", "classEnvironment.html", null ],
    [ "std::exception", null, [
      [ "std::runtime_error", null, [
        [ "file_error", "classfile__error.html", null ]
      ] ]
    ] ],
    [ "FCGIWriter", "classFCGIWriter.html", null ],
    [ "FileWriter", "classFileWriter.html", null ],
    [ "iip_destination_mgr", "structiip__destination__mgr.html", null ],
    [ "IIPImage", "classIIPImage.html", [
      [ "KakaduImage", "classKakaduImage.html", null ],
      [ "OpenJPEGImage", "classOpenJPEGImage.html", null ],
      [ "TPTImage", "classTPTImage.html", null ]
    ] ],
    [ "IIPResponse", "classIIPResponse.html", null ],
    [ "std::ios_base", null, [
      [ "std::basic_ios< Char >", null, [
        [ "std::basic_ostream< Char >", null, [
          [ "std::ostream", null, [
            [ "Logger", "classLogger.html", null ]
          ] ]
        ] ]
      ] ]
    ] ],
    [ "kdu_message", null, [
      [ "kdu_stream_message", "classkdu__stream__message.html", null ]
    ] ],
    [ "Memcache", "classMemcache.html", null ],
    [ "RawTile", "classRawTile.html", null ],
    [ "Session", "structSession.html", null ],
    [ "Task", "classTask.html", [
      [ "CMP", "classCMP.html", null ],
      [ "CNT", "classCNT.html", null ],
      [ "COL", "classCOL.html", null ],
      [ "CTW", "classCTW.html", null ],
      [ "CVT", "classCVT.html", null ],
      [ "DeepZoom", "classDeepZoom.html", null ],
      [ "FIF", "classFIF.html", null ],
      [ "GAM", "classGAM.html", null ],
      [ "HEI", "classHEI.html", null ],
      [ "ICC", "classICC.html", null ],
      [ "IIIF", "classIIIF.html", null ],
      [ "INV", "classINV.html", null ],
      [ "JTL", "classJTL.html", null ],
      [ "JTLS", "classJTLS.html", null ],
      [ "LYR", "classLYR.html", null ],
      [ "MINMAX", "classMINMAX.html", null ],
      [ "OBJ", "classOBJ.html", null ],
      [ "PFL", "classPFL.html", null ],
      [ "QLT", "classQLT.html", null ],
      [ "RGN", "classRGN.html", null ],
      [ "ROT", "classROT.html", null ],
      [ "SDS", "classSDS.html", null ],
      [ "SHD", "classSHD.html", null ],
      [ "SPECTRA", "classSPECTRA.html", null ],
      [ "TIL", "classTIL.html", null ],
      [ "WID", "classWID.html", null ],
      [ "Zoomify", "classZoomify.html", null ]
    ] ],
    [ "TileManager", "classTileManager.html", null ],
    [ "Timer", "classTimer.html", null ],
    [ "Tokenizer", "classTokenizer.html", null ],
    [ "Transform", "structTransform.html", null ],
    [ "URL", "classURL.html", null ],
    [ "View", "classView.html", null ],
    [ "Watermark", "classWatermark.html", null ],
    [ "Writer", "classWriter.html", null ]
];